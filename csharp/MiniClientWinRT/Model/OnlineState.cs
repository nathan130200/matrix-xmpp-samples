﻿namespace MiniClientWinRT.Model
{
    public enum OnlineState
    {
        Online,
        Offline,
        Away,
        ExtendedAway,
        DoNotDisturb,
        Chat
    }
}