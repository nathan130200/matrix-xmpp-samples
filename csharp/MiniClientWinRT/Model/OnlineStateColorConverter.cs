﻿using System;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace MiniClientWinRT.Model
{
    public class OnlineStateColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var state = (OnlineState)value;

            switch (state)
            {
                case OnlineState.Online:
                    return new SolidColorBrush(Colors.Green);
                case OnlineState.Offline:
                    return new SolidColorBrush(Colors.DarkGray);
                case OnlineState.Away:
                    return new SolidColorBrush(Colors.Blue);
                case OnlineState.ExtendedAway:
                    return new SolidColorBrush(Colors.DarkBlue);
                case OnlineState.DoNotDisturb:
                    return new SolidColorBrush(Colors.Red);
                case OnlineState.Chat:
                    return new SolidColorBrush(Colors.Yellow);
            }

            return new SolidColorBrush(Colors.LightGray);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
