﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using MiniClientWinRT.Model;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Sasl;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniClientWinRT
{
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // The demo requires a license key in order to run. If you have no license key yet
    // please request one and put it in the SetLicense funciton below.
    // You can request a evaluation license at:
    // http://www.ag-software.de/matrix-xmpp-sdk/request-demo-license/
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<Contact> Contacts = new ObservableCollection<Contact>();

        private XmppClient client;
        
        private Windows.UI.Text.FontWeight boldFont = new Windows.UI.Text.FontWeight() { Weight = 700 };
        private FontFamily debugFontFamily = new FontFamily("Courier New");

        
        public MainPage()
        {
            this.InitializeComponent();

            listContacts.ItemsSource = Contacts;

            SetLicense();
            InitXmppClient();
        }

        private void InitXmppClient()
        {
            client = new XmppClient
                         {
                             Dispatcher = Dispatcher,
                             AutoReplyToPing = true
                         };

            // hook up some events
            client.OnReceiveXml += client_OnReceiveXml;
            client.OnSendXml += client_OnSendXml;

            client.OnBeforeSasl += client_OnBeforeSasl;

            client.OnError += client_OnError;
            client.OnAuthError += client_OnAuthError;
            client.OnClose += client_OnClose;

            client.OnRosterStart += client_OnRosterStart;
            client.OnRosterItem += client_OnRosterItem;
            client.OnRosterEnd += client_OnRosterEnd;

            client.OnTls += client_OnTls;
            client.OnLogin += client_OnLogin;
            client.OnBind += client_OnBind;

            client.OnPresence += client_OnPresence;
            client.OnMessage += client_OnMessage;
            
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        
        #region << XmppClient events >>

        void client_OnError(object sender, Matrix.ExceptionEventArgs e)
        {
            DisplayEvent("OnError");
        }

        void client_OnRosterStart(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnRosterStart", "start receiving the roster");
        }

        void client_OnRosterItem(object sender, Matrix.Xmpp.Roster.RosterEventArgs e)
        {
            DisplayEvent("OnRosterItem", e.RosterItem.Jid, e.RosterItem.Name);

            if (e.RosterItem.Subscription != Matrix.Xmpp.Roster.Subscription.Remove)
                // contact removed
                Contacts.Add(
                    new Contact
                        {
                            Name = e.RosterItem.Name ?? e.RosterItem.Jid,
                            Jid = e.RosterItem.Jid
                        });
            else
            {
                // new or updated contacts
                var contact = Contacts.FirstOrDefault(c => c.Jid == e.RosterItem.Jid);
                if (contact != null)
                    Contacts.Remove(contact);
            }
        }

        void client_OnRosterEnd(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnRosterEnd", "end receiving the roster");
        }

        void client_OnLogin(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnLogin", "logged in (authenticated)");
        }

        void client_OnTls(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnTls", "transport channel is secure now");
        }

        void client_OnBind(object sender, Matrix.JidEventArgs e)
        {
            DisplayEvent("OnBind", "resource assigned");
        }

        void client_OnAuthError(object sender, SaslEventArgs e)
        {
            DisplayEvent("OnAuthError", "authentication failed");
        }

        void client_OnBeforeSasl(object sender, SaslEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => DisplayEvent("OnBeforeSasl"));
        }

        /// <summary>
        /// XMPP presence packet/stanza was received
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void client_OnPresence(object sender, PresenceEventArgs e)
        {
            DisplayEvent("OnPresence", e.Presence.From);
            string jid = e.Presence.From.Bare;
            var contact = Contacts.FirstOrDefault(c => c.Jid == jid);
            if (contact != null)
                contact.SetOnlineStateFromPresence(e.Presence);
        }

        /// <summary>
        /// XMPP message packet/stanza was received
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void client_OnMessage(object sender, MessageEventArgs e)
        {
            DisplayEvent("OnMessage", e.Message.From, e.Message.Body ?? "");
            
            if (e.Message.Body != null)
                InvokeSimpleToast(e.Message);
        }

        void client_OnClose(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnClose");
            Contacts.Clear();
            cmdDisconnect.IsEnabled = false;
            cmdConnect.IsEnabled = true;
        }

        void client_OnReceiveXml(object sender, Matrix.TextEventArgs e)
        {
            var par = new Paragraph();
            par.Inlines.Add(new Run
            {
                Text = "Recv: ",
                FontWeight = boldFont,
                FontSize = 14,
                FontFamily = debugFontFamily,
                Foreground = new SolidColorBrush(Colors.Red)
            });

            par.Inlines.Add(new Run
            {
                Text = e.Text,
                FontSize = 14,
                FontFamily = debugFontFamily,
            });

            rtfDebug.Blocks.Add(par);
            ScrollToEnd(scrollDebug);
        }

        void client_OnSendXml(object sender, Matrix.TextEventArgs e)
        {
            var par = new Paragraph();
            par.Inlines.Add(new Run
                                {
                                    Text = "Send: ",
                                    FontWeight = boldFont,
                                    FontSize = 14,
                                    FontFamily = debugFontFamily,
                                    Foreground = new SolidColorBrush(Colors.Blue)
                                });

            par.Inlines.Add(new Run
                                {
                                    Text = e.Text,
                                    FontSize = 14,
                                    FontFamily = debugFontFamily,
                                });
        
            rtfDebug.Blocks.Add(par);
            ScrollToEnd(scrollDebug);
        }
        #endregion

        void DisplayEvent(string evt, string arg1 = null, string arg2 = null)
        {
            var par = new Paragraph();
            par.Inlines.Add(new Run
                                {
                                    Text = evt,
                                    FontWeight = boldFont,
                                    FontSize = 14,
                                    FontFamily = debugFontFamily,
                                });

            if (arg1 != null)
                par.Inlines.Add(new Run
                                    {
                                        Text = "\t=>" + arg1,
                                        FontSize = 14,
                                        FontFamily = debugFontFamily,
                                    });

            if (arg2 != null)
                par.Inlines.Add(new Run
                                    {
                                        Text =  "\t=> " + arg2,
                                        FontSize = 14,
                                        FontFamily = debugFontFamily,
                                    });
            
            rtfEvents.Blocks.Add(par);
            ScrollToEnd(scrollEvents);
        }

        private void InvokeSimpleToast(Message msg)
        {
            var toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastImageAndText02);

            // You can use the methods from the XML document to specify all of the
            // required parameters for the toast
            var stringElements = toastXml.GetElementsByTagName("text");
            stringElements.Item(0).AppendChild(toastXml.CreateTextNode("Message from: " + msg.From));
            stringElements.Item(1).AppendChild(toastXml.CreateTextNode(msg.Body));

            // Audio tags are not included by default, so must be added to the XML document.
            const string audioSrc = "ms-winsoundevent:Notification.IM";
            var audioElement = toastXml.CreateElement("audio");
            audioElement.SetAttribute("src", audioSrc);

            var toastNode = toastXml.SelectSingleNode("/toast");
            toastNode.AppendChild(audioElement);

            // Create a toast from the Xml, then create a ToastNotifier object to show the toast.
            var toast = new ToastNotification(toastXml);

            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void ScrollToEnd(ScrollViewer scroll)
        {
            scroll.UpdateLayout();
            scroll.ScrollToVerticalOffset(double.MaxValue);
        }

        void SetLicense()
        {
			// request a evaluation license at:
			// http://www.ag-software.de/matrix-xmpp-sdk/request-demo-license/
            const string LIC = @"PUT YOUR LICENSE KEY HERE";

            Matrix.License.LicenseManager.SetLicense(LIC);
        }

        #region << Button Click events >>
        private void cmdConnect_Click(object sender, RoutedEventArgs e)
        {
            cmdConnect.IsEnabled = false;
            cmdDisconnect.IsEnabled = true;

            client.XmppDomain = txtXmppDomain.Text;
            client.Username = txtUsername.Text;
            client.Password = txtPassword.Text;

            if (!String.IsNullOrEmpty(txtHostname.Text))
                client.Hostname = txtHostname.Text;

            client.Open();
        }

        private void cmdDisconnect_Click(object sender, RoutedEventArgs e)
        {
            client.Close();
        }

        #endregion

        #region << radio Click Event for TabControl simulation >>
        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            var radBtn = sender as RadioButton;
            if (radBtn != null)
            {
                int index = Convert.ToInt32(radBtn.Tag);
                _flipVw.SelectedIndex = index;
            }
        }
        #endregion
    }
}
