﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Roster;

namespace SilverLightClient
{
    public partial class Chat : UserControl
    {
        XmppClient      _xmppClient;
        bool            _shiftKey;

        internal Jid    _jid;
        internal string _nickname;


        #region << Constructors >>
        public Chat(XmppClient client, Jid jid, string nickname)
        {
            Global.Chats.Add(this);

            InitializeComponent();
            
            _xmppClient = client;            
            _jid        = jid;
            _nickname   = nickname;
        }
        
        public Chat(XmppClient client, RosterItem ri) : this(client, ri.Jid, ri.Name)
        {            
        }
        #endregion

        /// <summary>
        /// Handles the Click event of the SendButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            SendChat();
        }

        /// <summary>
        /// Gets the nickname.
        /// </summary>
        /// <param name="jid">The jid.</param>
        /// <param name="nickname">The nickname.</param>
        /// <returns></returns>
        private string GetNickname(string jid, string nickname)
        {
            return string.IsNullOrEmpty(nickname) ? jid : nickname;
        }

        private string NickName
        {
            get { return GetNickname(_jid.Bare, _nickname); }
        }

        internal void pw_Closed(object sender, RoutedEventArgs e)
        {
            Global.Chats.Remove(this);
        }

        /// <summary>
        /// Display a incoming message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public void IncomingChat(Message msg)
        {
            Run run1 = new Run() { Text = NickName + " sais: ", FontWeight = FontWeights.Bold, Foreground = new SolidColorBrush(Colors.Blue) };
            Run run2 = new Run() { Text = msg.Body };

            if (txtIncoming.Inlines.Count > 0)
                txtIncoming.Inlines.Add(new LineBreak());
            
            txtIncoming.Inlines.Add(run1);
            txtIncoming.Inlines.Add(run2);

            scrollIn.ScrollToVerticalOffset(double.MaxValue);            
        }

        /// <summary>
        /// Display a outgoing message.
        /// </summary>
        /// <param name="text">The text.</param>
        private void OutgoingChat(string text)
        {
            Run run1 = new Run() { Text = "I say: ", FontWeight = FontWeights.Bold, Foreground = new SolidColorBrush(Colors.Red) };
            Run run2 = new Run() { Text = text };

            if (txtIncoming.Inlines.Count > 0)
                txtIncoming.Inlines.Add(new LineBreak());

            txtIncoming.Inlines.Add(run1);
            txtIncoming.Inlines.Add(run2);

            scrollIn.ScrollToVerticalOffset(double.MaxValue);
        }

        /// <summary>
        /// Handles the KeyUp event of the txtOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void txtOut_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Shift)
                _shiftKey = false;

            if (_shiftKey && e.Key == Key.Enter)
            {
                txtOut.Text = txtOut.Text + Environment.NewLine;
                txtOut.SelectionStart = txtOut.Text.Length;
                scrollOut.ScrollToVerticalOffset(double.MaxValue);
            }

            if (!_shiftKey && e.Key == Key.Enter)
                SendChat();
        }

        /// <summary>
        /// Handles the KeyDown event of the txtOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void txtOut_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Shift)
                _shiftKey = true;            
        }

        /// <summary>
        /// Sends the chat.
        /// </summary>
        private void SendChat()
        {
            // only send when there is text
            if (string.IsNullOrEmpty(txtOut.Text))
                return;

            Message msg = new Message();
            msg.To = _jid;
            msg.Type = MessageType.chat;
            msg.Body = txtOut.Text;

            _xmppClient.Send(msg);

            OutgoingChat(txtOut.Text);
            txtOut.Text = "";
        }        
    }
}