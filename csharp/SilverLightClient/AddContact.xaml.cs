﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Matrix;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Roster;

namespace SilverLightClient
{
    public partial class AddContact : UserControl
    {             
        XmppClient _xmppClient;
        public AddContact(XmppClient xmppClient)
        {
            InitializeComponent();            

            _xmppClient = xmppClient;
        }

        private void cmdCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void cmdAdd_Click(object sender, RoutedEventArgs e)
        {
            RosterManager rm    = new RosterManager(_xmppClient);
            PresenceManager pm  = new PresenceManager(_xmppClient);

            Jid jid = txtJid.Text;

            rm.Add(jid, txtNickname.Text);
            pm.Subscribe(jid);

            this.Close();
        }

        /// <summary>
        /// Closes this window.
        /// </summary>
        private void Close()
        {
            ((PopupWindow)this.Parent).Close();
        }
    }
}