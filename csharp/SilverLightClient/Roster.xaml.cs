﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Roster;

namespace SilverLightClient
{
    /// <summary>
    /// Roster user control
    /// </summary>
    public partial class Roster : UserControl
    {
        PresenceManager _presManager;
        XmppClient      _xmppClient;
        bool            _isDebugWindowOpen;
        bool            _isAddContactWindowOpen;

        #region << Constructor >>
        /// <summary>
        /// Initializes a new instance of the <see cref="Roster"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public Roster(XmppClient client)
        {
            InitializeComponent();

            ShowLogin();

            _xmppClient = client;
            _presManager = new PresenceManager(_xmppClient);

            _xmppClient.OnRosterStart += new EventHandler<Matrix.EventArgs>(_xmppClient_OnRosterStart);
            _xmppClient.OnRosterItem += new EventHandler<RosterEventArgs>(_xmppClient_OnRosterItem);
            _xmppClient.OnRosterEnd += new EventHandler<Matrix.EventArgs>(_xmppClient_OnRosterEnd);
            _xmppClient.OnClose += new EventHandler<Matrix.EventArgs>(_xmppClient_OnClose);
            _xmppClient.OnMessage += new EventHandler<MessageEventArgs>(_xmppClient_OnMessage);

            _presManager.OnAvailablePresence += new EventHandler<PresenceEventArgs>(presManager_OnAvailablePresence);
            _presManager.OnUnavailablePresence += new EventHandler<PresenceEventArgs>(presManager_OnUnavailablePresence);
            _presManager.OnSubscribe += new EventHandler<PresenceEventArgs>(presManager_OnSubscribe);
        }
        #endregion

        #region << events >>

        #region << PresenceManager events >>
        /// <summary>
        /// A contact goes offline
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.Xmpp.Client.PresenceEventArgs"/> instance containing the event data.</param>
        void presManager_OnUnavailablePresence(object sender, PresenceEventArgs e)
        {
            // no Invoke required, does the library for us            
            roster.SetPresence(e.Presence, false);
        }
        
        /// <summary>
        /// we get a available presence from a contact
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.Xmpp.Client.PresenceEventArgs"/> instance containing the event data.</param>
        void presManager_OnAvailablePresence(object sender, PresenceEventArgs e)
        {
            // no Invoke required, does the library for us
            roster.SetPresence(e.Presence, true);
        }

        /// <summary>
        /// triggers when a contact wants to subscribe to our presence
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.Xmpp.Client.PresenceEventArgs"/> instance containing the event data.</param>
        void presManager_OnSubscribe(object sender, PresenceEventArgs e)
        {
            // no Invoke required, does the library for us
            Presence pres = e.Presence;

            PopupWindow pw = new PopupWindow();
            pw.Title = "Subscription request";
            pw.Width = 400;
            pw.Height = 250;
            pw.SetWindowOffset();
            pw.IsModal = false;

            pw.Content = new Subscribe(_xmppClient, pres.From, roster.Contains(pres.From));

            Page.AddPopup(pw);
        }
        #endregion

        #region << XmppClient events >>

        #region << Roster events >>
        /// <summary>
        /// We start to receive the "full" roster (contact list) now
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.EventArgs"/> instance containing the event data.</param>
        void _xmppClient_OnRosterStart(object sender, Matrix.EventArgs e)
        {
            /* 
             * you can use this event to enable and disable redraw while receiving the roster
             * this speeds up the UI code on big contact lists.
             */
        }

        /// <summary>
        /// This event triggers on contact updates
        /// * we receive a new contact, or contact from our contact list
        /// * a contact update
        /// * a contact was removed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.Xmpp.Roster.RosterEventArgs"/> instance containing the event data.</param>
        void _xmppClient_OnRosterItem(object sender, Matrix.Xmpp.Roster.RosterEventArgs e)
        {
            // no Invoke required, does the library for us
            if (e.RosterItem.Subscription == Subscription.remove)
                roster.RemoveContact(e.RosterItem);
            else
                roster.AddContact(e.RosterItem);
        }

        /// <summary>
        /// The "full" roster (contact list) is received now
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.EventArgs"/> instance containing the event data.</param>
        void _xmppClient_OnRosterEnd(object sender, Matrix.EventArgs e)
        {
            /* 
             * you can use this event to enable and disable redraw while receiving the roster
             * this speeds up the UI code on big contact lists.
             */

            // no Invoke required, does the library for us
            cboStatus.IsEnabled = true;
            cboStatus.SelectedItem = cboItemStatusOnline;
        }
        #endregion

        
        /// <summary>
        /// triggers when the XMPP session gets closed/terminated
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.EventArgs"/> instance containing the event data.</param>
        void _xmppClient_OnClose(object sender, Matrix.EventArgs e)
        {
            // no Invoke required, does the library for us
            roster.Clear();
            ShowLogin();
            cboStatus.IsEnabled = false;
            cboStatus.SelectedItem = cboItemStatusOffline;
        }

        /// <summary>
        /// triggers when a message stanza gets received
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Matrix.Xmpp.Client.MessageEventArgs"/> instance containing the event data.</param>
        void _xmppClient_OnMessage(object sender, MessageEventArgs e)
        {
            // no Invoke required, does the library for us
            Message msg = e.Message;
            if (msg.Body != null)
            {
                if (!Global.ChatExists(msg.From))
                {
                    // TODO, find nickname in roster
                    OpenNewChatWindow(_xmppClient, msg, null);
                }
                else
                {
                    Global.GetChat(msg.From).IncomingChat(msg);
                }
            }
        }
        #endregion

        #region << UI events >>
        /// <summary>
        /// Handles the Click event of the LoginButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        void Login_Click(object sender, RoutedEventArgs e)
        {
            Connect();
            ShowRoster();
        }

        /// <summary>
        /// Handles the SelectionChanged event of the cboStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        void cboStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_xmppClient != null && _xmppClient.StreamActive)
            {
                Presence pres = new Presence();
                ComboBoxItem cboItem = cboStatus.SelectedItem as ComboBoxItem;

                switch ((string)cboItem.Tag)
                {
                    // change show type to online
                    case "online":
                        _xmppClient.SendPresence(Show.NONE);
                        break;
                    // go offline (unavailabe)
                    case "offline":
                        _xmppClient.SendUnavailablePresence();
                        _xmppClient.Close();
                        break;
                    // change show type to free for chat
                    case "chat":
                        _xmppClient.SendPresence(Show.chat);
                        break;
                    // change show type to away
                    case "away":
                        _xmppClient.SendPresence(Show.away);
                        break;
                    // change show type to extended away
                    case "xa":
                        _xmppClient.SendPresence(Show.xa);
                        break;
                    // change show type to do not disturb
                    case "dnd":
                        _xmppClient.SendPresence(Show.dnd);
                        break;
                }
            }
        }
        /// <summary>
        /// Handles the Click event of the AddContactButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void AddContactButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isAddContactWindowOpen)
                return;

            PopupWindow pw = new PopupWindow();
            pw.Title = "Add contact";
            pw.Width = 300;
            pw.Height = 200;
            pw.SetWindowOffset();
            pw.IsModal = false;
            pw.Content = new AddContact(_xmppClient);
            pw.Closed += delegate(object osender, RoutedEventArgs oe) { _isAddContactWindowOpen = false; };

            Page.AddPopup(pw);
            _isAddContactWindowOpen = true;
        }

        /// <summary>
        /// Handles the Click event of the DebugButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void DebugButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isDebugWindowOpen)
                return;

            PopupWindow pw = new PopupWindow();
            pw.Title = "Debug Window";
            pw.Width = 300;
            pw.Height = 300;
            pw.SetWindowOffset();
            pw.IsModal = false;
            pw.Content = new Debug(_xmppClient);
            pw.Closed += delegate(object osender, RoutedEventArgs oe) { _isDebugWindowOpen = false; };

            Page.AddPopup(pw);
            _isDebugWindowOpen = true;

        }

        /// <summary>
        /// Handles the Click event of the MessageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MessageButton_Click(object sender, RoutedEventArgs e)
        {
            ContactListItem selectedItem = roster.listRoster.SelectedItem as ContactListItem;
            if (selectedItem != null && !Global.ChatExists(selectedItem.Jid))
            {
                OpenNewChatWindow(_xmppClient, selectedItem.Jid, selectedItem.Name);
            }
        }
        #endregion
        #endregion
        
        #region << private functions >>
        /// <summary>
        /// Shows the login control.
        /// </summary>
        private void ShowLogin()
        {
            login.Visibility = Visibility.Visible;
            roster.Visibility = Visibility.Collapsed;

            gridRowLogin.Height     = new GridLength(1, GridUnitType.Star);
            gridRowRoster.Height    = new GridLength(0, GridUnitType.Star);            
        }

        /// <summary>
        /// Shows the roster control.
        /// </summary>
        private void ShowRoster()
        {
            login.Visibility = Visibility.Collapsed;
            roster.Visibility = Visibility.Visible;

            gridRowLogin.Height     = new GridLength(0, GridUnitType.Star);
            gridRowRoster.Height    = new GridLength(1, GridUnitType.Star);
        }       

        /// <summary>
        /// Connects this instance.
        /// </summary>
        private void Connect()
        {
            /*
             * set your  provided license key here
             *  
             * request demo licnese from here:
             * http://www.ag-software.net/matrix-xmpp-sdk/request-demo-license/
			*/
			string lic = @"Put your evaluation key here";

            Matrix.License.LicenseManager.SetLicense(lic);            
            

            _xmppClient.Username = login.Jid.User;
            _xmppClient.Password = login.Password;
            
            _xmppClient.XmppDomain = login.Jid.Server;
            
            _xmppClient.Port = 4502;

            if (!String.IsNullOrEmpty(login.Hostname))
                _xmppClient.Hostname = login.Hostname;
            
            _xmppClient.AutoPresence = false;
            _xmppClient.Priority = login.Priority;
            
            /*
             * One restriction on using sockets in Silverlight 2 is that the port range that a network application 
             * is allowed to connect to must be within the range of 4502-4534.
             * These are the only ports allowed for connection using sockets from Silverlight 2 applications.
             * If a connection is to a port is not within this port range, the connection attempt will fail.
             * 
             * If you connect over HTTP Proxy (see below) then this restriction does not apply to the Xmpp Port
             * but it does to the ProxyPort.
             */
            _xmppClient.Port = 5222;
            //_xmppClient.Port = 4503;

            /*
             * if you want to use Bosh then set the tarnsport to Bosh
             * an the Uri to the correct Bosh Uri
             * 
            
            xmppCon.Transport = Matrix.Net.Tranport.BOSH;
            xmppCon.Uri = new System.Uri("http://server.com/http-bind");
             */         

            // status and show for Autopresence after login
            _xmppClient.Status  = "online";
            _xmppClient.Show    = Show.NONE;

            /*
             * we connect through a HTTP Proxy (HTTP Tunnel) here.
             * So the Silverlight port range restriction applies to the ProxyPort, but not to the xmpp port.
             */
            _xmppClient.ProxyType       = Matrix.Net.Proxy.ProxyType.HttpTunnel;
            //_xmppClient.ProxyHostname = "matrix.ag-software.de";
            _xmppClient.ProxyHostname   = "localhost";
            _xmppClient.ProxyPort       = 4503;

            _xmppClient.Open();
        }

        private string GetNickname(string jid, string nickname)
        {
            return string.IsNullOrEmpty(nickname) ? jid : nickname;
        }

        private Chat OpenNewChatWindow(XmppClient client, Jid jid, string nickname)
        {
            ChatPopupWindow pw = new ChatPopupWindow(jid);
            pw.Title = "Chat - " + GetNickname(jid, nickname);
            pw.Width = 300;
            pw.Height = 300;
            pw.OffsetX = 100 + 1 * 10;
            pw.OffsetY = 100 + 1 * 10;
            pw.IsModal = false;

            Chat chat = new Chat(client, jid, nickname);
            pw.Content = chat;

            pw.Closed += new RoutedEventHandler(chat.pw_Closed);

            Page.AddPopup(pw);

            return chat;
        }

        private void OpenNewChatWindow(XmppClient client, Message msg, string nickname)
        {
            OpenNewChatWindow(client, msg.From, nickname).IncomingChat(msg);
        }
        #endregion
    }
}