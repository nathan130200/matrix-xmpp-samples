﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Matrix;

namespace SilverLightClient
{
    /// <summary>
    /// LoginControl
    /// </summary>
    public partial class LoginControl : UserControl
    {
        public event RoutedEventHandler LoginClick;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginControl"/> class.
        /// </summary>
        public LoginControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the jid.
        /// </summary>
        /// <value>The jid.</value>
        public Jid Jid
        {
            get { return txtJid.Text;}
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password
        {
          get { return txtPassword.Password;}
        }

        /// <summary>
        /// Gets the hostname.
        /// </summary>
        /// <value>The hostname.</value>
        public string Hostname
        {
            get { return txtHostname.Text; }
        }

        /// <summary>
        /// Gets the priority.
        /// </summary>
        /// <value>The priority.</value>
        public int Priority
        {
            get { return (int) txtPriority.Value; }
        }

        /// <summary>
        /// Handles the Click event of the cmdLogin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void cmdLogin_Click(object sender, RoutedEventArgs e)
        {
            if (LoginClick != null)
                LoginClick(sender, e);
        }
    }
}
