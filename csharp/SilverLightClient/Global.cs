﻿using System;
using System.Linq;

using System.Collections;
using System.Collections.Generic;

using Matrix;

namespace SilverLightClient
{
    /// <summary>
    /// Global
    /// </summary>
    public class Global
    {
        /// <summary>
        /// Colelction of open ChatWindows
        /// </summary>
        public static List<Chat> Chats = new List<Chat>();

        /// <summary>
        /// is a chat window with the given Jid active?
        /// </summary>
        /// <param name="jid"></param>
        /// <returns></returns>
        public static bool ChatExists(Jid jid)
        {
            return Chats.Any(c=>c._jid.Equals(jid, new BareJidComparer()));
        }

        /// <summary>
        /// Gets the chat window for the given Jid.
        /// </summary>
        /// <param name="jid">The jid.</param>
        /// <returns></returns>
        public static Chat GetChat(Jid jid)
        {
            return Chats.First(c=>c._jid.Equals(jid, new BareJidComparer()));
        }        
    }
}
