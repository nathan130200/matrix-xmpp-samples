﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

using System.Net;
using System.Net.Sockets;

using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Roster;

namespace SilverLightClient
{

    /// <summary>
    /// Page
    /// </summary>
    public partial class Page : UserControl
    {
        XmppClient xmppCon = new XmppClient();
        
        private Roster dlgRoster;
        private static Grid _layoutRoot;

        /// <summary>
        /// Initializes a new instance of the <see cref="Page"/> class.
        /// </summary>
        public Page()
        {            
            InitializeComponent();            
            
            _layoutRoot = LayoutRoot;
            ShowContactList();
        }

        /// <summary>
        /// Shows the contact list window.
        /// </summary>
        private void ShowContactList()
        {
            PopupWindow pw = new PopupWindow();
            pw.Title = "MatriX";
            pw.Width = 300;
            pw.Height = 350;
            pw.SetWindowOffset();
            pw.IsModal = false;
            pw.Content = new Roster(xmppCon);
            pw.ShowClose = false;

            AddPopup(pw);            
        }

        /// <summary>
        /// Adds the popup.
        /// </summary>
        /// <param name="pw">The pw.</param>
        public static void AddPopup(PopupWindow pw)
        {
            _layoutRoot.Children.Add(pw);
        }     
    }
}