﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;


using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Roster;

namespace SilverLightClient
{
    /// <summary>
    /// 
    /// </summary>
    public partial class RosterControl : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public class RosterCollection : ObservableCollection<ContactListItem>
        {           
        }       
        
        internal RosterCollection colRoster = new RosterCollection();

        /// <summary>
        /// Initializes a new instance of the <see cref="RosterControl"/> class.
        /// </summary>
        public RosterControl()
        {
            InitializeComponent();

            listRoster.ItemsSource = colRoster;
        }

        /// <summary>
        /// Determines whether [contains] [the specified jid].
        /// </summary>
        /// <param name="jid">The jid.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified jid]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(Jid jid)
        {
            return colRoster.FirstOrDefault(item => item.Jid.Equals(jid.Bare)) == null ? false : true;
        }

        /// <summary>
        /// Removes the contact.
        /// </summary>
        /// <param name="ri">The ri.</param>
        public void RemoveContact(RosterItem ri)
        {            
            // check first if contact exists
            ContactListItem cli = colRoster.FirstOrDefault(item => item.Jid.Equals(ri.Jid.Bare));
            
            if (cli != null)
                colRoster.Remove(cli);            
        }

        /// <summary>
        /// Adds the contact.
        /// </summary>
        /// <param name="ri">The ri.</param>
        public void AddContact(RosterItem ri)
        {
            // check first if contact exists
            ContactListItem cli = colRoster.FirstOrDefault(item => item.Jid.Equals(ri.Jid.Bare));
            if (cli != null)
                cli.Update(ri);
            else
                colRoster.Add(new ContactListItem(ri));
        }

        /// <summary>
        /// Sets the presence.
        /// </summary>
        /// <param name="pres">The pres.</param>
        /// <param name="available">if set to <c>true</c> [available].</param>
        public void SetPresence(Presence pres, bool available)
        {
            ContactListItem cli = colRoster.FirstOrDefault(item => item.Jid.Equals(pres.From.Bare));
                     
            if (cli != null)
            {
                string status = pres.Status;
                if (status != null)
                    cli.Status = status;
                else
                    cli.Status = "";

                if (!available)
                {
                    // Contact goes offline
                    cli.StatusImage = StatusImage.Offline;
                }
                else
                {
                    // Contact online
                    switch (pres.Show)
                    {
                        case Show.NONE:
                            cli.StatusImage = StatusImage.Online;
                            break;
                        case Show.chat:
                            cli.StatusImage = StatusImage.Chat;
                            break;
                        case Show.away:
                            cli.StatusImage = StatusImage.Away;
                            break;
                        case Show.xa:
                            cli.StatusImage = StatusImage.Xa;
                            break;
                        case Show.dnd:
                            cli.StatusImage = StatusImage.Dnd;
                            break;
                    }
                }
            }
        }      

        public void Clear()
        {
            colRoster.Clear();
        }
    }
}