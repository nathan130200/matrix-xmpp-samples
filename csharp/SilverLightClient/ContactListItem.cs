﻿using System;
using System.ComponentModel;

using Matrix.Xmpp.Roster;

namespace SilverLightClient
{
    /// <summary>
    /// ContactListItem
    /// </summary>
    public class ContactListItem : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        // Declare the PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged;

        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)        
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));            
        }
        #endregion
        
        internal RosterItem _rosterItem;
        
        private string      m_Status        = "";
        private StatusImage m_StatusImage   = StatusImage.Offline;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactListItem"/> class.
        /// </summary>
        /// <param name="ri">The ri.</param>
        public ContactListItem(RosterItem ri)
        {
            _rosterItem = ri;
        }

        /// <summary>
        /// Updates the ContactListItem with the data of the new RosterItem
        /// </summary>
        /// <param name="ri">The ri.</param>
        public void Update(RosterItem ri)
        {
            _rosterItem = ri;
            NotifyPropertyChanged("Name");
            NotifyPropertyChanged("Display");
            NotifyPropertyChanged("Jid");
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return _rosterItem.Name; }
            set 
            {
                _rosterItem.Name = value; 
                NotifyPropertyChanged("Name");
                //NotifyPropertyChanged("Jid"); 
            }
        }

        /// <summary>
        /// Gets the display name for the roster.
        /// </summary>
        /// <value>The display.</value>
        public string Display
        {
            get 
            {
                return String.IsNullOrEmpty(Name) ? Jid : Name;                
            }           
        }

        /// <summary>
        /// Gets the jid.
        /// </summary>
        /// <value>The jid.</value>
        public string Jid
        {
            get { return _rosterItem.Jid.Bare; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status
        {
            get { return m_Status; }
            set
            {
                m_Status = value;
                NotifyPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Gets or sets the status image.
        /// </summary>
        /// <value>The status image.</value>
        public StatusImage StatusImage
        {
            get { return m_StatusImage; }
            set 
            { 
                m_StatusImage = value;
                NotifyPropertyChanged("StatusImage");
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is ContactListItem))
                return false;
            else
                return ((ContactListItem)obj)._rosterItem.Jid.Equals(_rosterItem.Jid);
        }
    }
}