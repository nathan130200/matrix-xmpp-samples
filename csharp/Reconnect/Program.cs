using System;

namespace Reconnect
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix.License.LicenseManager.SetLicense(@"ENTER YOUR KEY HERE");

            // TODO set your credentials here
            var xmppWrapper = new ReconnectXmppWrapper("ag-software.de", "test", "secret");
            xmppWrapper.Connect(null);
            
            Console.WriteLine("press any key to stop");
            Console.ReadLine();
        }
    }
}