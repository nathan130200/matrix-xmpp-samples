﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Coding4Fun.Phone.Controls;
using Matrix;
using Matrix.Xmpp.Sasl;
using Microsoft.Phone.Controls;

using Matrix.Xmpp;
using Matrix.Xmpp.Client;
using MiniClientWP.Model;
using Uri = System.Uri;


namespace MiniClientWP
{
    public partial class MainPage : PhoneApplicationPage
    {
        public ObservableCollection<Contact> Contacts = new ObservableCollection<Contact>();
        private FontWeight boldFont = FontWeights.Bold;
        private FontFamily debugFontFamily = new FontFamily("Courier New");

        XmppClient xmppClient;
        
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            listRoster.ItemsSource = Contacts;

            xmppClient = new XmppClient();
            SetupXmppHandlers();
            xmppClient.Dispatcher = Dispatcher;
        }

      
        void SetupXmppHandlers()
        {
            xmppClient.OnReceiveXml += xmppClient_OnReceiveXml;
            xmppClient.OnSendXml += xmppClient_OnSendXml;

            xmppClient.OnBeforeSasl += xmppClient_OnBeforeSasl;

            xmppClient.OnError += xmppClient_OnError;
            xmppClient.OnAuthError += xmppClient_OnAuthError;
            xmppClient.OnClose += xmppClient_OnClose;

            xmppClient.OnRosterStart += xmppClient_OnRosterStart;
            xmppClient.OnRosterItem += xmppClient_OnRosterItem;
            xmppClient.OnRosterEnd += xmppClient_OnRosterEnd;

            xmppClient.OnTls += xmppClient_OnTls;
            xmppClient.OnLogin += xmppClient_OnLogin;
            xmppClient.OnBind += xmppClient_OnBind;

            xmppClient.OnPresence += xmppClient_OnPresence;
            xmppClient.OnMessage += xmppClient_OnMessage;
        }


        /// <summary>
        /// Enter your MatriX license key yet.
        /// If you have not purchases a license yet you can request a 30 days evaluation license key here:
        /// http://www.ag-software.de/matrix-xmpp-sdk/request-demo-license/
        /// </summary>
        void SetLicense()
        {
            const  string LIC = @"Your personal license key";

            Matrix.License.LicenseManager.SetLicense(LIC);
        }
        

        private void cmdConnect_Click(object sender, RoutedEventArgs e)
        {
            cmdConnect.IsEnabled = false;
            cmdDisconnect.IsEnabled = true;

            ConnectXmpp();
        }

        private void cmdDisconnect_Click(object sender, RoutedEventArgs e)
        {
            xmppClient.Close();
        }

        void ConnectXmpp()
        {
            SetLicense();
            
            xmppClient.XmppDomain = txtXmppDomain.Text;
            xmppClient.Username = txtUsername.Text;
            xmppClient.Password = txtPassword.Text;

            if (!String.IsNullOrEmpty(txtHostname.Text))
                xmppClient.Hostname = txtHostname.Text;

            // if you want to use the BOSH transport insead of sockets
            //xmppClient.Transport = Matrix.Net.Transport.BOSH;
            //xmppClient.Uri = new System.Uri("http://localhost/http-bind/");
            

            xmppClient.Open();
        }

        void xmppClient_OnError(object sender, Matrix.ExceptionEventArgs e)
        {
            DisplayEvent("OnError");
        }

        void xmppClient_OnRosterEnd(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnRosterEnd", "end receiving the roster");
        }

        void xmppClient_OnRosterStart(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnRosterStart", "start receiving the roster");
        }

        void xmppClient_OnLogin(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnLogin", "logged in (authenticated)");
        }

        void xmppClient_OnTls(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnTls", "transport channel is secure now");
        }

        void xmppClient_OnBind(object sender, Matrix.JidEventArgs e)
        {
            DisplayEvent("OnBind", "resource assigned");
        }

        void xmppClient_OnAuthError(object sender, SaslEventArgs e)
        {
            DisplayEvent("OnAuthError", "authentication failed");
        }

        void xmppClient_OnBeforeSasl(object sender, SaslEventArgs e)
        {
            Dispatcher.BeginInvoke(() => DisplayEvent("OnBeforeSasl"));
        }

        void xmppClient_OnPresence(object sender, PresenceEventArgs e)
        {
            DisplayEvent("OnPresence", e.Presence.From);
            string jid = e.Presence.From.Bare;
            var contact = Contacts.FirstOrDefault(c => c.Jid == jid);
            if (contact != null)
                contact.SetOnlineStateFromPresence(e.Presence);
        }

        void xmppClient_OnRosterItem(object sender, Matrix.Xmpp.Roster.RosterEventArgs e)
        {
            DisplayEvent("OnRosterItem", e.RosterItem.Jid, e.RosterItem.Name);
            if (e.RosterItem.Subscription != Matrix.Xmpp.Roster.Subscription.remove)
                Contacts.Add(
                    new Contact
                    {
                        Name = e.RosterItem.Name ?? e.RosterItem.Jid,
                        Jid = e.RosterItem.Jid
                    });
            else
            {
                var contact = Contacts.FirstOrDefault(c => c.Jid == e.RosterItem.Jid);
                if (contact != null)
                    Contacts.Remove(contact);
            }
        }

        void DisplayEvent(string evt, string arg1 = null, string arg2 = null)
        {
            var par = new Paragraph();
            par.Inlines.Add(new Run
            {
                Text = evt,
                FontWeight = boldFont,
                FontSize = 14,
                FontFamily = debugFontFamily,
            });

            if (arg1 != null)
                par.Inlines.Add(new Run
                {
                    Text = "\t=>" + arg1,
                    FontSize = 14,
                    FontFamily = debugFontFamily,
                });

            if (arg2 != null)
                par.Inlines.Add(new Run
                {
                    Text = "\t=> " + arg2,
                    FontSize = 14,
                    FontFamily = debugFontFamily,
                });

            var rtf = new RichTextBox();
            rtf.Blocks.Add(par);
            stackEvents.Children.Add(rtf);
            ScrollToEnd(scrollEvents);
        }

        void xmppClient_OnMessage(object sender, MessageEventArgs e)
        {
            DisplayEvent("OnMessage", e.Message.From);
            if (e.Message.Body != null)
                MessageBox.Show("Message from: " + e.Message.From.Bare, e.Message.Body, MessageBoxButton.OK);
        }


        void xmppClient_OnClose(object sender, Matrix.EventArgs e)
        {
            DisplayEvent("OnClose");
            Contacts.Clear();
            cmdDisconnect.IsEnabled = false;
            cmdConnect.IsEnabled = true;
        }

        void xmppClient_OnSendXml(object sender, Matrix.TextEventArgs e)
        {
            Debug.WriteLine("SEND:" + e.Text);

            var par = new Paragraph();
            par.Inlines.Add(new Run
            {
                Text = "Send: ",
                FontWeight = boldFont,
                FontSize = 14,
                FontFamily = debugFontFamily,
                Foreground = new SolidColorBrush(Colors.Blue)
            });

            par.Inlines.Add(new Run
            {
                Text = e.Text,
                FontSize = 14,
                FontFamily = debugFontFamily,
            });
            // we add a new RichtextBox for every messsage because of the 2048px
            // restiction of any control in WP
            var rtf = new RichTextBox();
            rtf.Blocks.Add(par);
            stackDebug.Children.Add(rtf);
            ScrollToEnd(scrollDebug);
        }

        void xmppClient_OnReceiveXml(object sender, Matrix.TextEventArgs e)
        {
            var par = new Paragraph();
            par.Inlines.Add(new Run
            {
                Text = "Recv: ",
                FontWeight = boldFont,
                FontSize = 14,
                FontFamily = debugFontFamily,
                Foreground = new SolidColorBrush(Colors.Red)
            }
                );

            par.Inlines.Add(new Run
            {
                Text = e.Text,
                FontSize = 14,
                FontFamily = debugFontFamily,
            });

            // we add a new RichtextBox for every messsage because of the 2048px
            // restiction of any control in WP   
            var rtf = new RichTextBox();
            rtf.Blocks.Add(par);
            stackDebug.Children.Add(rtf);
            ScrollToEnd(scrollDebug);
        }

        private void ScrollToEnd(ScrollViewer scroll)
        {
            scroll.UpdateLayout();
             scroll.ScrollToVerticalOffset(double.MaxValue);
        }

        private void sendMesssageMenu_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            if (menuItem != null)
            {
                var contact = menuItem.DataContext as Contact;
                if (contact != null)
                {
                    var jid = contact.Jid;

 
                    var input = new InputPrompt();
                    input.Completed += (s, ea) =>
                        {
                            var msg = new Message
                                {
                                    To = jid, Body = input.Value, 
                                    Type = MessageType.chat
                                };
                            xmppClient.Send(msg);
                        };
                    input.Title = "Send message to " + jid;
                    input.Message = "Enter your message here";
                    input.Show();
                }
            }
        }


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    
    }
}