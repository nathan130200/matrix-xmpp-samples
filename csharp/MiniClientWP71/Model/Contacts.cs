﻿using System.ComponentModel;
using Matrix.Xmpp.Client;

namespace MiniClientWP.Model
{
    public class Contact : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Contact()
        {
            OnlineState = OnlineState.Offline;
        }

        public string Name {get; set;}
        public string Jid { get; set; }
        public string Status { get; set; }
        public OnlineState OnlineState { get; set; }

        public void SetOnlineStateFromPresence(Presence pres)
        {
            if (pres.Type == Matrix.Xmpp.PresenceType.unavailable)
                OnlineState = OnlineState.Offline;

            if (pres.Type == Matrix.Xmpp.PresenceType.available)
            {
                if (pres.Show == Matrix.Xmpp.Show.chat)
                    OnlineState = OnlineState.Chat;
                else if (pres.Show == Matrix.Xmpp.Show.away)
                    OnlineState = OnlineState.Away;
                else if (pres.Show == Matrix.Xmpp.Show.xa)
                    OnlineState = OnlineState.ExtendedAway;
                else if (pres.Show == Matrix.Xmpp.Show.dnd)
                    OnlineState = OnlineState.DoNotDisturb;
                else
                    OnlineState = OnlineState.Online;
            }

            Status = pres.Status ?? "";
            OnPropertyChanged("Status");
            OnPropertyChanged("OnlineState");
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
