﻿namespace MiniClientWP.Model
{
    public enum OnlineState
    {
        Online,
        Offline,
        Away,
        ExtendedAway,
        DoNotDisturb,
        Chat
    }
}