﻿using System;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Google.Mobile;
using Newtonsoft.Json.Linq;

namespace FirebaseClient
{
    class Program
    {
        static string senderId      = "YOUR_FIREBASE_SENDER_ID";
        static string serverKey     = "YOUR_FIREBASE_SERVER_KEY";
        static string testDeviceId  = "YOUR_TEST_DEVICE_ID";

        static XmppClient xmppClient = new XmppClient();

        static void SetLicense()
        {
            string lic = @"ADD_YOUR_LICENSE_KEY_HERE";

            Matrix.License.LicenseManager.SetLicense(lic);
        }
        
        static void Main(string[] args)
        {
            // set your MatriX license first
            SetLicense();

            xmppClient.XmppDomain   = "gcm.googleapis.com";
            xmppClient.Hostname     = "fcm-xmpp.googleapis.com";
            xmppClient.Port         = 5236;

            xmppClient.ResolveSrvRecords = false;
            xmppClient.OldStyleSsl = true;
        
            xmppClient.Username = senderId;     // your sender id
            xmppClient.Password = serverKey;    // your server key

            xmppClient.AutoRoster   = false;
            xmppClient.AutoPresence = false;

            xmppClient.OnReceiveXml += XmppClient_OnReceiveXml;
            xmppClient.OnSendXml    += XmppClient_OnSendXml;
            xmppClient.OnMessage    += XmppClient_OnMessage;
            xmppClient.OnLogin      += XmppClient_OnLogin;

            xmppClient.Open();

            Console.WriteLine("Press return key to exit the application");
            Console.ReadLine();

            xmppClient.Close();
        }

        private static void XmppClient_OnLogin(object sender, Matrix.EventArgs e)
        {
            // we send a Gcm message here after we are authenticated

            /*
             * sample message from Google docs
             {
                 'notification':{'title':'Portugal vs. Denmark','body':'5 to 1'},
                 'message_id':'some-id',
                 'to':'some-device'
             }
             */

            var jsonMessgae = JObject.FromObject(new
            {
                notification = new
                {
                    title   = "Portugal vs. Denmark",
                    body    = "5 to 1"
                },
                message_id = Guid.NewGuid().ToString(),
                to = testDeviceId
            });

            var msg = new Message { Id = Guid.NewGuid().ToString() };
            msg.Add(new Gcm { Value = jsonMessgae.ToString() });

            xmppClient.Send(msg);
        }

        private static void XmppClient_OnMessage(object sender, MessageEventArgs e)
        {
            // received a GCM message
            Console.WriteLine("EVENT: OnMessage");
        }

        private static void XmppClient_OnSendXml(object sender, Matrix.TextEventArgs e)
        {
            Console.WriteLine($"SEND: {e.Text}");
        }

        private static void XmppClient_OnReceiveXml(object sender, Matrix.TextEventArgs e)
        {
            Console.WriteLine($"RECV: {e.Text}");
        }
    }
}