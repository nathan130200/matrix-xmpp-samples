﻿using System;
using System.ComponentModel;
using Matrix;

namespace MiniClientWP
{
    public class Contact : INotifyPropertyChanged
    {
        private string mName = null;
        private Jid mJid = null;
        
        public event PropertyChangedEventHandler PropertyChanged;
        
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public string Name
        {
            get { return mName; }
            set
            {
                if (value != mName)
                {
                    mName = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public Jid Jid
        {
            get { return mJid; }
            set
            {
                if (value != mJid)
                {
                    mJid = value;
                    NotifyPropertyChanged("Jid");
                }
            }
        }
    }
}