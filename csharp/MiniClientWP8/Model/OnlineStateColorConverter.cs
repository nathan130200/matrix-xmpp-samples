﻿using System;
using System.Windows.Data;
using System.Windows.Media;


namespace MiniClientWP8.Model
{
    public class OnlineStateColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var state = (OnlineState)value;

            switch (state)
            {
                case OnlineState.Online:
                    return new SolidColorBrush(Colors.Green);
                case OnlineState.Offline:
                    return new SolidColorBrush(Colors.Black);
                case OnlineState.Away:
                    return new SolidColorBrush(Colors.Blue);
                case OnlineState.ExtendedAway:
                    return new SolidColorBrush(Colors.DarkGray);
                case OnlineState.DoNotDisturb:
                    return new SolidColorBrush(Colors.Red);
                case OnlineState.Chat:
                    return new SolidColorBrush(Colors.Yellow);
            }

            return new SolidColorBrush(Colors.LightGray);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
