﻿namespace MiniClientWP8.Model
{
    public enum OnlineState
    {
        Online,
        Offline,
        Away,
        ExtendedAway,
        DoNotDisturb,
        Chat
    }
}