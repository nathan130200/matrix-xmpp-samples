﻿using System.ComponentModel;
using Matrix.Xmpp.Client;

namespace MiniClientWP8.Model
{
    public class Contact : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Contact()
        {
            OnlineState = OnlineState.Offline;
        }

        public string Name {get; set;}
        public string Jid { get; set; }
        public string Status { get; set; }
        public OnlineState OnlineState { get; set; }

        public void SetOnlineStateFromPresence(Presence pres)
        {
            if (pres.Type == Matrix.Xmpp.PresenceType.Unavailable)
                OnlineState = OnlineState.Offline;

            if (pres.Type == Matrix.Xmpp.PresenceType.Available)
            {
                if (pres.Show == Matrix.Xmpp.Show.Chat)
                    OnlineState = OnlineState.Chat;
                else if (pres.Show == Matrix.Xmpp.Show.Away)
                    OnlineState = OnlineState.Away;
                else if (pres.Show == Matrix.Xmpp.Show.ExtendedAway)
                    OnlineState = OnlineState.ExtendedAway;
                else if (pres.Show == Matrix.Xmpp.Show.DoNotDisturb)
                    OnlineState = OnlineState.DoNotDisturb;
                else
                    OnlineState = OnlineState.Online;
            }

            Status = pres.Status ?? "";
            OnPropertyChanged("Status");
            OnPropertyChanged("OnlineState");
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
