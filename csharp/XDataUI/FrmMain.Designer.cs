﻿namespace XDataUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabResult = new System.Windows.Forms.TabPage();
            this.txtResultXml = new System.Windows.Forms.TextBox();
            this.tabFormData = new System.Windows.Forms.TabPage();
            this.txtFormXml = new System.Windows.Forms.TextBox();
            this.cmdRenderUI = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.xDataControl1 = new Matrix.Ui.XData.XDataControl();
            this.tabResult.SuspendLayout();
            this.tabFormData.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabResult
            // 
            this.tabResult.Controls.Add(this.txtResultXml);
            this.tabResult.Location = new System.Drawing.Point(4, 22);
            this.tabResult.Name = "tabResult";
            this.tabResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabResult.Size = new System.Drawing.Size(342, 375);
            this.tabResult.TabIndex = 1;
            this.tabResult.Text = "Result Xml";
            this.tabResult.UseVisualStyleBackColor = true;
            // 
            // txtResultXml
            // 
            this.txtResultXml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultXml.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultXml.Location = new System.Drawing.Point(3, 3);
            this.txtResultXml.Multiline = true;
            this.txtResultXml.Name = "txtResultXml";
            this.txtResultXml.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResultXml.Size = new System.Drawing.Size(336, 369);
            this.txtResultXml.TabIndex = 1;
            // 
            // tabFormData
            // 
            this.tabFormData.Controls.Add(this.cmdRenderUI);
            this.tabFormData.Controls.Add(this.txtFormXml);
            this.tabFormData.Location = new System.Drawing.Point(4, 22);
            this.tabFormData.Name = "tabFormData";
            this.tabFormData.Padding = new System.Windows.Forms.Padding(3);
            this.tabFormData.Size = new System.Drawing.Size(342, 375);
            this.tabFormData.TabIndex = 0;
            this.tabFormData.Text = "Data Form Xml";
            this.tabFormData.UseVisualStyleBackColor = true;
            // 
            // txtFormXml
            // 
            this.txtFormXml.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFormXml.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFormXml.Location = new System.Drawing.Point(6, 4);
            this.txtFormXml.Multiline = true;
            this.txtFormXml.Name = "txtFormXml";
            this.txtFormXml.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFormXml.Size = new System.Drawing.Size(330, 326);
            this.txtFormXml.TabIndex = 0;
            // 
            // cmdRenderUI
            // 
            this.cmdRenderUI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdRenderUI.Location = new System.Drawing.Point(6, 336);
            this.cmdRenderUI.Name = "cmdRenderUI";
            this.cmdRenderUI.Size = new System.Drawing.Size(107, 27);
            this.cmdRenderUI.TabIndex = 1;
            this.cmdRenderUI.Text = "Render UI";
            this.cmdRenderUI.UseVisualStyleBackColor = true;
            this.cmdRenderUI.Click += new System.EventHandler(this.cmdRenderUI_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl.Controls.Add(this.tabFormData);
            this.tabControl.Controls.Add(this.tabResult);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(350, 401);
            this.tabControl.TabIndex = 0;
            // 
            // xDataControl1
            // 
            this.xDataControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xDataControl1.From = null;
            this.xDataControl1.Location = new System.Drawing.Point(368, 34);
            this.xDataControl1.Name = "xDataControl1";
            this.xDataControl1.ShowButtons = true;
            this.xDataControl1.ShowLegend = true;
            this.xDataControl1.Size = new System.Drawing.Size(292, 379);
            this.xDataControl1.TabIndex = 1;
            this.xDataControl1.OnOk += new System.EventHandler(this.xDataControl1_OnOk);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 422);
            this.Controls.Add(this.xDataControl1);
            this.Controls.Add(this.tabControl);
            this.Name = "FrmMain";
            this.Text = "XData user interface example";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.tabResult.ResumeLayout(false);
            this.tabResult.PerformLayout();
            this.tabFormData.ResumeLayout(false);
            this.tabFormData.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Matrix.Ui.XData.XDataControl xDataControl1;
        private System.Windows.Forms.TabPage tabResult;
        private System.Windows.Forms.TextBox txtResultXml;
        private System.Windows.Forms.TabPage tabFormData;
        private System.Windows.Forms.Button cmdRenderUI;
        private System.Windows.Forms.TextBox txtFormXml;
        private System.Windows.Forms.TabControl tabControl;
    }
}

